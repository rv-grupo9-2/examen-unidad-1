package com.example.examenunidad1_kotlin

class CuentaBanco {
    private var numCuenta = 0
    private var nombre: String? = null
    private var banco: String? = null
    private var saldo = 0f

    // Constructor de parámetros
    fun CuentaBanco(numCuenta: Int, nombre: String?, banco: String?, saldo: Float) {
        this.numCuenta = numCuenta
        this.nombre = nombre
        this.banco = banco
        this.saldo = saldo
    }

    fun setSaldo(saldo: Float) {
        this.saldo = saldo
    }

    fun getSaldo():Float {
        return saldo
    }

    // Función depositar
    fun depositar(saldoD: Float) {
        // Introducir el saldo depositado
        this.saldo = this.saldo + saldoD
    }

    fun retirar(saldoR: Float): Boolean {
        // Caso donde el saldo es insuficiente para retirar
        if(saldoR > this.saldo){
            return false
        }
        // Caso donde el saldo es suficiente para retirar
        this.saldo = this.saldo - saldoR
        return true
    }
}