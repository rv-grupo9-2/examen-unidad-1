package com.example.examenunidad1_kotlin

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog

class MainActivity : AppCompatActivity() {
    private lateinit var txtNumCuenta: EditText
    private lateinit var txtNombre: EditText
    private lateinit var txtBanco: EditText
    private lateinit var txtSaldo: EditText

    private lateinit var btnEnviar: Button
    private lateinit var btnSalir: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        iniciarComponentes()

        // Envento click de los botones
        btnEnviar.setOnClickListener { enviar() }
        btnSalir.setOnClickListener { salir() }
    }

    private fun iniciarComponentes() {
        // Cajas de texto
        txtNumCuenta = findViewById(R.id.txtNumCuenta)
        txtNombre = findViewById(R.id.txtNombre)
        txtBanco = findViewById(R.id.txtBanco)
        txtSaldo = findViewById(R.id.txtSaldo)

        // Botones
        btnEnviar = findViewById(R.id.btnEnviar)
        btnSalir = findViewById(R.id.btnSalir)
    }

    private fun enviar() {
        if(txtNumCuenta.text.toString().isEmpty() || txtNombre.text.toString().isEmpty() ||
                txtBanco.text.toString().isEmpty() || txtSaldo.text.toString().isEmpty()){
            Toast.makeText(
                this.applicationContext, "Introduce los datos faltantes",
                Toast.LENGTH_SHORT).show()
        } else {
            // Hacer el paquete para enviar la información
            var bundle = Bundle()
            bundle.putString("banco", txtBanco.text.toString())
            bundle.putString("nombre", txtNombre.text.toString())
            bundle.putString("saldo",  txtSaldo.text.toString())

            // Crear el Intent para llamar a otra actividad
            var intent = Intent(this@MainActivity, CuentaBancoActivity::class.java)
            intent.putExtras(bundle)

            // Iniciar la actividad esperando respuesta o no
            startActivity(intent)
        }
    }

    private fun salir(){
        val confirmar = AlertDialog.Builder(this)
        confirmar.setTitle("Cuenta de Banco")
        confirmar.setMessage("¿Desea salir de la app?")
        confirmar.setPositiveButton("Confirmar") { dialog, which -> finish() }
        confirmar.setNegativeButton("Cancelar") { dialog, which ->  }
        confirmar.show()
    }
}